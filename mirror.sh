#!/bin/bash
# GTFS mirroring tool.  This allows us to keep the GTFS data from Public
# Transport Victoria into a revision control system (so we could look up
# historical data).
#
# Copyright 2011,2015 Michael Farrell <http://micolous.id.au>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

DATA_ZIP="http://data.ptv.vic.gov.au/downloads/gtfs.zip"
DATA_ZIPFILENAME="gtfs.zip"

# Create the target folder
mkdir -p "gtfs"

# Download data archive
# Normally -O would be used here, but it doesn't work in combination with -N.
# (see wget man page for details)
wget -N "${DATA_ZIP}"

# Extract the data into the data folder
unzip -ouL "${DATA_ZIPFILENAME}" -d gtfs

# Each aency has their own ZIP inside of the ZIP which needs to be extracted.
# There is no agency 9 in Melbourne, as it was John Batman's unlucky number.
for agency in 1 2 3 4 5 6 7 8 10 11
do
	mkdir -p "gtfs/${agency}"
	python shittypack/shittypack.py -o "gtfs/${agency}/shittypack.zip" "gtfs/${agency}/google_transit.zip"
	unzip -oujL "gtfs/${agency}/shittypack.zip" -d "gtfs/${agency}"
	git add gtfs/${agency}/*.txt
done

# Commit
DATE="`date`"
MSG="New data from upstream source on ${DATE}"

git commit -am "${MSG}"

# Push downstream to github
git push origin master
