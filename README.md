GTFS mirroring tool.  This allows us to keep the GTFS data from Public
Transit Victoria in a revision control system (so we could look up
historical data).

It should be trivial to adapt this to monitor another public transit
system.

This is best used in a daily crontab.

You shouldn't need to run this script yourself though, I already provide a
repository at <https://bitbucket.org/micolous/ptvictoria-gtfs>.  It has data
starting from March 2015.  PTV started providing this data
publicly in March 2015.


Copyright 2011,2015 Michael Farrell <http://micolous.id.au>

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

The data provided (in the `gtfs` folder) is licensed under [Creative Commons
Australia Attribution 3.0
License](http://creativecommons.org/licenses/by/3.0/au/deed.en).

This repository uses the tool
[shittypack](https://github.com/micolous/shittypack) in order to
automatically reformat the contents of this dataset.  This generally
improves the quality of the source dataset by removing redundant, useless or
verbose data.  However, it is done without any human quality analysis, and
may be subject to additional errors not in the source data.

Public Transit Victoria divide the service areas into the following subfolders:

* 1: Regional Trains
* 2: Metropolitan Trains
* 3: Metropolitan Trams
* 4: Metropolitan Buses
* 5: Regional Coaches
* 6: Regional Buses
* 7: TeleBus
* 8: NightRider
* 9 is not used
* 10: Interstate
* 11: SkyBus (Tullamarine Airport - City)

A current version of the data held in this repository is available for free
from data.vic.gov.au, at
<https://www.data.vic.gov.au/data/dataset/ptv-timetable-and-geographic-information-2015-gtfs>.

