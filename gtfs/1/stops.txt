stop_id,stop_name,stop_lat,stop_lon
40,Wallan Railway Station (Wallan),-37.416861,145.005372
62,Melton Railway Station (Melton South),-37.703293,144.572524
66,Rockbank Railway Station (Rockbank),-37.729261,144.650631
63,Deer Park Railway Station (Deer Park),-37.777763,144.772303
17,Sunbury Railway Station (Sunbury),-37.579206,144.728164
64,Ardeer Railway Station (Ardeer),-37.783065,144.802188
87,Craigieburn Railway Station (Craigieburn),-37.601924,144.943306
15,Southern Cross Railway Station (Melbourne City),-37.818334,144.952524
47,Albury Railway Station (Albury (NSW)),-36.084261,146.924515
56,Ararat Railway Station (Ararat),-37.282204,142.936913
55,Avenel Railway Station (Avenel),-36.893647,145.229514
61,Bacchus Marsh Railway Station (Maddingley),-37.687578,144.436785
0,Bairnsdale Railway Station (Bairnsdale),-37.828720,147.627613
60,Ballan Railway Station (Ballan),-37.604299,144.225448
59,Ballarat Railway Station (Ballarat Central),-37.558791,143.859457
57,Beaufort Railway Station (Beaufort),-37.427874,143.382233
52,Benalla Railway Station (Benalla),-36.544550,145.983915
27,Bendigo Railway Station (Bendigo),-36.765669,144.283008
94,Birregurra Railway Station (Birregurra),-38.328807,143.783625
37,Broadford Railway Station (Broadford),-37.207200,145.043007
70,Bunyip Railway Station (Bunyip),-38.099107,145.720757
96,Camperdown Railway Station (Camperdown),-38.228900,143.150927
25,Castlemaine Railway Station (Castlemaine),-37.062837,144.213799
49,Chiltern Railway Station (Chiltern),-36.155636,146.611374
18,Clarkefield Railway Station (Clarkefield),-37.483497,144.745372
95,Colac Railway Station (Colac),-38.343377,143.586653
80,Corio Railway Station (Corio),-38.072831,144.379768
88,Dingee Railway Station (Dingee),-36.369232,144.231120
44,Donnybrook Railway Station (Donnybrook),-37.542589,144.969860
8,Drouin Railway Station (Drouin),-38.136451,145.855946
74,Eaglehawk Railway Station (Eaglehawk),-36.718525,144.248379
31,Echuca Railway Station (Echuca),-36.130982,144.753458
29,Elmore Railway Station (Elmore),-36.495020,144.607757
54,Euroa Railway Station (Euroa),-36.749145,145.567864
9,Garfield Railway Station (Garfield),-38.091030,145.674224
83,Geelong Railway Station (Geelong),-38.144242,144.354989
20,Gisborne Railway Station (New Gisborne),-37.458825,144.598721
43,Heathcote Junction Railway Station (Heathcote Junction),-37.367734,145.026310
26,Kangaroo Flat Railway Station (Kangaroo Flat),-36.794833,144.248990
90,Kerang Railway Station (Kerang),-35.733121,143.924425
38,Kilmore East Railway Station (Kilmore East),-37.293210,144.983565
23,Kyneton Railway Station (Kyneton),-37.258278,144.450601
79,Lara Railway Station (Lara),-38.022427,144.414404
78,Little River Railway Station (Little River),-37.962929,144.498484
69,Longwarry Railway Station (Longwarry),-38.112838,145.771095
21,Macedon Railway Station (Macedon),-37.423573,144.561396
24,Malmsbury Railway Station (Malmsbury),-37.189740,144.375323
85,Marshall Railway Station (Marshall),-38.198549,144.355056
6,Moe Railway Station (Moe),-38.176714,146.262697
33,Mooroopna Railway Station (Mooroopna),-36.399128,145.358214
5,Morwell Railway Station (Morwell),-38.236718,146.396753
34,Murchison East Railway Station (Murchison East),-36.613147,145.240635
35,Nagambie Railway Station (Nagambie),-36.785464,145.160356
72,Nar Nar Goon Railway Station (Nar Nar Goon),-38.081591,145.571665
82,North Geelong Railway Station (North Geelong),-38.122835,144.352271
81,North Shore Railway Station (North Shore),-38.098377,144.365321
89,Pyramid Railway Station (Pyramid Hill),-36.053112,144.113132
19,Riddells Creek Railway Station (Riddells Creek),-37.465123,144.679830
30,Rochester Railway Station (Rochester),-36.362360,144.698681
3,Rosedale Railway Station (Rosedale),-38.156403,146.786966
2,Sale Railway Station (Sale),-38.103102,147.054805
36,Seymour Railway Station (Seymour),-37.024728,145.137728
32,Shepparton Railway Station (Shepparton),-36.383791,145.406497
84,South Geelong Railway Station (South Geelong),-38.158658,144.358987
50,Springhurst Railway Station (Springhurst),-36.185893,146.470416
1,Stratford Railway Station (Stratford),-37.967047,147.081469
91,Swan Hill Railway Station (Swan Hill),-35.341113,143.562343
42,Tallarook Railway Station (Tallarook),-37.092334,145.102997
97,Terang Railway Station (Terang),-38.236211,142.911472
67,Trafalgar Railway Station (Trafalgar),-38.207242,146.154772
4,Traralgon Railway Station (Traralgon),-38.198884,146.537882
71,Tynong Railway Station (Tynong),-38.084996,145.628234
53,Violet Town Railway Station (Violet Town),-36.638816,145.715923
39,Wandong Railway Station (Wandong),-37.354677,145.026415
51,Wangaratta Railway Station (Wangaratta),-36.355100,146.317038
7,Warragul Railway Station (Warragul),-38.165224,145.933085
99,Warrnambool Railway Station (Warrnambool),-38.385013,142.475545
93,Winchelsea Railway Station (Winchelsea),-38.240135,143.984133
48,Wodonga Railway Station (Wodonga),-36.105827,146.871265
22,Woodend Railway Station (Woodend),-37.358798,144.525890
68,Yarragon Railway Station (Yarragon),-38.203157,146.063062
14,Flinders Street Railway Station (Melbourne City),-37.818305,144.966964
46,North Melbourne Railway Station (West Melbourne),-37.806309,144.941510
16,Footscray Railway Station (Footscray),-37.801413,144.902020
65,Sunshine Railway Station (Sunshine),-37.788536,144.832878
75,Watergardens Railway Station (Sydenham),-37.701128,144.774180
13,Richmond Railway Station (Richmond),-37.824074,144.990164
12,Caulfield Railway Station (Caulfield East),-37.877459,145.042524
73,Clayton Railway Station (Clayton),-37.924682,145.120534
11,Dandenong Railway Station (Dandenong),-37.989967,145.209725
92,Berwick Railway Station (Berwick),-38.040408,145.345714
10,Pakenham Railway Station (Pakenham),-38.080613,145.486367
45,Essendon Railway Station (Essendon),-37.756011,144.916197
41,Broadmeadows Railway Station (Broadmeadows),-37.683049,144.919613
98,Sherwood Park Railway Station (Warrnambool),-38.386393,142.540564
58,Wendouree Railway Station (Wendouree),-37.540085,143.822054
100,Creswick Railway Station (Creswick),-37.424605,143.888123
101,Clunes Railway Station (Clunes),-37.303140,143.781576
103,Maryborough Railway Station (Maryborough),-37.050908,143.742402
102,Talbot Railway Station (Talbot),-37.172899,143.705640
86,Waurn Ponds Railway Station (Waurn Ponds),-38.215814,144.306819
28,Epsom Railway Station (Epsom),-36.706341,144.321039
77,Wyndham Vale Railway Station (Wyndham Vale),-37.871781,144.608472
76,Tarneit Railway Station (Tarneit),-37.832168,144.694714
